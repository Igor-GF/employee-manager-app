# Simple Employee Manager (backend)

Full Stack Java/Angular application aimed to get knowledge with JAVA / Angular stack.
App built based on AmigosCode tutorial om https://www.youtube.com/watch?v=Gx4iBLKLVHk

## Link to the frontend project
https://...

## Technologies
- Java Spring Boot (Maven library)
- HTML
- CSS
- TypeScript
- Angular
- MySQL DB

## Built With
[IntelliJ IDEA](https://www.jetbrains.com/idea/)

## author
[Igor Figueiredo](https://github.com/Igor-GF)

## Credits
[Amigoscode](https://www.youtube.com/c/amigoscode)

## Project status
Project exclusively made for study purposes.

## License
[MIT](https://choosealicense.com/licenses/mit/)